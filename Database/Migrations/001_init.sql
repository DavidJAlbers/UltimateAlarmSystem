USE ultimate_alarm_system;

CREATE TABLE IF NOT EXISTS hw_components
(
    cID   INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    type  VARCHAR(50),
    name  VARCHAR(50),
    other VARCHAR(255)
);
