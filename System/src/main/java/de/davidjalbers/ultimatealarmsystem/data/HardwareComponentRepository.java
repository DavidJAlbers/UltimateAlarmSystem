package de.davidjalbers.ultimatealarmsystem.data;

import de.davidjalbers.ultimatealarmsystem.api.WebServiceResult;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Diese Utility-Klasse kümmert sich um die Interaktion mit der Datenbank.
 *
 * Zunächst sollte {@link #setupConnection(String, String...)} aufgerufen werden, um die erste Verbindung mit der
 * Datenbank herzustellen. Von dort an kümmert sich die Klasse automatisch darum, dass immer eine valide Verbindung
 * zur Datenbank besteht.
 *
 * Die statische Methode {@link #getHardwareComponents()} liefert dann zum Beispiel die vollständige Liste aller
 * Komponenten. Das erwartete Datenbankschema orientiert sich an der Klasse {@link HardwareComponent}:
 *
 * <ol>
 *     <li> cID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT</li>
 *     <li> type  VARCHAR(50)</li
 *     <li> name  VARCHAR(50)</li>
 *     <li> other VARCHAR(255)</li>
 * </ol>
 */
public final class HardwareComponentRepository {

    // Instanziierung verhindern
    private HardwareComponentRepository() {}

    private static Connection currentConnection;

    private static String tableName;
    private static String[] connectionStrings;

    /**
     * Stellt die Verbindung zur Datenbank mit den Hardwarekomponenten her.
     * @param tableName der Name der Tabelle in der Datenbank
     * @param connectionStrings Verbindungsstrings im JDBC-Format. Mehrere Strings werden in der Reihenfolge des
     *                          Übergebens ausprobiert, bis die Verbindung hergestellt ist. Dies ist zum Beispiel
     *                          nützlich, wenn die Datenbankhosts sich von Umgebung zu Umgebung unterscheiden (etwa im
     *                          Docker-Container oder localhost, etc.).
     */
    public static void setupConnection(String tableName, String... connectionStrings) {
        HardwareComponentRepository.tableName = tableName;
        HardwareComponentRepository.connectionStrings = connectionStrings;
        for (String connectionString : connectionStrings) {
            if (attemptConnection(connectionString))
                break;
        }
        if (currentConnection == null)
            throw new IllegalStateException("All database connection strings failed (Did you make a typo, or is the database down?)");
    }

    /**
     * Liest die Datenbank und gibt dort augeführte Hardwarekomponenten als Liste zurück.
     *
     * Für das Datenbankschema siehe {@link HardwareComponentRepository}.
     *
     * @return die Hardwarekomponenten in der Datenbank
     */
    public static List<HardwareComponent> getHardwareComponents() {
        return safeGuard(() -> {
            ResultSet queryResult = currentConnection.createStatement().executeQuery("SELECT * FROM " + tableName);
            List<HardwareComponent> components = new LinkedList<>();
            while (queryResult.next()) {
                components.add(new HardwareComponent(
                        queryResult.getInt(1),
                        queryResult.getString(2),
                        queryResult.getString(3),
                        queryResult.getString(4)
                ));
            }
            return components;
        });
    }

    public static HardwareComponent getHardwareComponent(int cID) {
        return safeGuard(() -> {
            PreparedStatement statement = currentConnection.prepareStatement("SELECT * FROM " + tableName + " WHERE cID = ?");
            statement.setInt(1, cID);
            ResultSet queryResult = statement.executeQuery();
            return new HardwareComponent(
                    queryResult.getInt(1),
                    queryResult.getString(2),
                    queryResult.getString(3),
                    queryResult.getString(4)
            );
        });
    }

    public static boolean deleteHardwareComponent(int cID) {
        return safeGuard(() -> {
            PreparedStatement statement = currentConnection.prepareStatement("DELETE FROM " + tableName + " WHERE cID = ?");
            statement.setInt(1, cID);
            statement.execute();
            return true;
        });
    }

    public static boolean updateHardwareComponent(int cID, String type, String name, String other) {
        return safeGuard(() -> {
               PreparedStatement statement = currentConnection.prepareStatement("UPDATE " + tableName + " SET type = ?, name = ?, other = ? WHERE cID = ?");
               statement.setString(1, type);
               statement.setString(2, name);
               statement.setString(3, other);
               statement.setInt(4, cID);
               statement.execute();
               return true;
        });
    }

    public static boolean addHardwareComponent(String type, String name, String other) {
        return safeGuard(() -> {
            PreparedStatement statement = currentConnection.prepareStatement("INSERT INTO " + tableName + "(type, name, other) VALUES (?, ?, ?)");
            statement.setString(1, type);
            statement.setString(2, name);
            statement.setString(3, other);
            statement.execute();
            return true;
        });
    }

    private static <T> T safeGuard(Callable<T> task) {
        try {
            if (!currentConnection.isValid(0)) {
                // Neu aufbauen
                setupConnection(tableName, connectionStrings);
            }
            return task.call();
        } catch (Exception exception) {
            throw new RuntimeException("Database query execution failed", exception);
        }
    }

    private static boolean attemptConnection(String connectionString) {
        boolean success;
        try {
            currentConnection = DriverManager.getConnection(connectionString);
            success = true;
        } catch (SQLException ignored) {
            success = false;
        }
        return success;
    }
}
