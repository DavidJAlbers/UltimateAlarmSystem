package de.davidjalbers.ultimatealarmsystem.components;

import de.davidjalbers.ultimatealarmsystem.IView;

/**
 * Eine View fuer Testzwecke, die ein getimetes Ereignis auslöst.
 * Nach einer spezifizierten Zeit wird ein Ereignis simuliert und ein zuvor gesetztes Runnable aufgerufen.
 */
public class DummyView implements IView {

    private final Runnable requestHandler;

    private final int triggerDelay;

    private boolean done = false;

    private final long creationTimeStamp = System.currentTimeMillis();

    /**
     * Erzeugt einen neuen {@code DummyButtonView} mit einem Ereignishandler.
     * @param requestHandler der Ereignishandler, an den das simulierte Ereignis gemeldet werden soll
     * @param triggerDelay die bis zum Auslösen des Ereignisses abzuwartende Zeitspanne, in Sekunden
     */
    public DummyView(Runnable requestHandler, int triggerDelay) {
        this.requestHandler = requestHandler;
        this.triggerDelay = triggerDelay;
    }

    @Override
    public void poll() {
        if (!done) {
            if (System.currentTimeMillis() - creationTimeStamp > triggerDelay * 1000) {
                requestHandler.run();
                done = true;
            }
        }
    }
}
