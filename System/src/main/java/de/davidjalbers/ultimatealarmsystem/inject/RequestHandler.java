package de.davidjalbers.ultimatealarmsystem.inject;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Definiert die mit dieser Annotation annotierte Methode als RequestHandler für bestimmte Views.
 * Die Methode darf keine Rückgabe haben (void) und keine Parameter entgegennehmen.
 *
 * Wird diese Methode im Rahmen der {@link ComponentService#setupViews(Object)}-Methode gefunden, dann wird sie
 * als "Listener" für die Views mit den jeweiligen Namen registriert (siehe {@link #views()}). Sie wird dann aufgerufen,
 * wenn die entsprechende View ein Ereignis ausgeloest hat.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RequestHandler {

    /**
     * Gibt die Namen der Views zurück, für die diese Methode den RequestHandler darstellt.
     * @return die Views dieses RequestHandlers
     */
    String[] views();
}
