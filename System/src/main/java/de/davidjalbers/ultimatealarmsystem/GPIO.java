package de.davidjalbers.ultimatealarmsystem;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Ein Wrapper um die Pi4J Library.
 */
public final class GPIO {

    // Instanziierung verhindern
    private GPIO() {}

    private static boolean isRaspbian;

    /**
     * Gibt den GpioController von Pi4J zurueck.
     * @return den GpioController von Pi4J
     * @throws RuntimeException falls diese Methode nicht auf einem Raspberry Pi aufgerufen wird
     */
    public static GpioController getController() {
        if (!isRaspbian) {
            checkRaspbian();
        }
        return GpioFactory.getInstance();
    }

    private static void checkRaspbian() {
        try {
            StringBuilder osRelease = new StringBuilder();
            for (String line : Files.readAllLines(Path.of("/etc/os-release"))) {
                osRelease.append(line).append('\n');
            }
            if (!osRelease.toString().contains("Raspbian"))
                throw new RuntimeException("Not running on Raspbian");
            isRaspbian = true;
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Not running on Linux", e);
        }
    }

}
