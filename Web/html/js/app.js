//////////////////////////////////////////////// für index.html ///////////////////////////////////////////////////////

/**
 * Aktualisiert den Status des Alarmsystems über die Web-API.
 * Setzt Text und Hintergrundfarbe der Statusboxen.
 */
function refreshStatus() {
    $.ajax({
        url: `${getBaseURL()}/status`,
        error: function (request, status, error) {
            alert('Das Alarmsystem ist anscheinend offline. Die Verbindung wiederherstellen, um das Control Panel verwenden zu können.');
            throw new Error('System offline');
        }
    }).then(function(data) {
        const armStatus = $("#arm-status");
        const triggerStatus = $("#trigger-status");

        // Feld für die Scharfstellung
        if (!data.armed) {
            armStatus.removeClass("btn-success");
            armStatus.addClass("btn-danger");
            triggerStatus[0].style.visibility = 'hidden';
            $("#arm-status-text").html("nicht scharfgestellt");
        } else {
            armStatus.removeClass("btn-danger");
            armStatus.addClass("btn-success");
            triggerStatus[0].style.visibility = 'visible';
            $("#arm-status-text").html("scharfgestellt");
        }
        // Feld für den Alarm
        if (data.triggered) {
            triggerStatus.removeClass("bg-success");
            triggerStatus.addClass("btn btn-danger");
            $("#trigger-status-text").html("ausgelöst");
            $('#trigger-status-extra').html('(tippen, um auszuschalten)');
        } else {
            triggerStatus.removeClass("btn btn-danger");
            triggerStatus.addClass("bg-success");
            $("#trigger-status-text").html("nicht ausgelöst");
            $('#trigger-status-extra').html('');
        }
    })
}

/**
 * Shortcut, um phpMyAdmin auf demselben Host zu öffnen.
 */
function phpmyadmin() {
    window.open(`http://${window.location.host}:3307`);
}

/**
 * Startet den Timer, der regelmäßig auf Änderungen des Alarmanlagenstatus überprüft und mit refreshStatus()
 * diesen geändert Status anzeigt.
 */
function scheduleStatusUpdate() {
    // Erstes Mal
    refreshStatus();
    // Das Aktualisieren passiert ab jetzt jede Viertel Sekunde
    setInterval(refreshStatus, 250);
}


////////////////////////////////////////// für hardware.html /////////////////////////////////////////////////////////

/**
 * Fügt eine Komponente zur Hardwareliste hinzu, indem die Textfelder im Tabellenfooter ausgelesen und an die API
 * übersandt werden. Rendert anschließend die Hardwareliste mit listHardware() neu, sodass die neue Komponente direkt
 * angezeigt werden kann.
 */
function addComponent() {
    const type = document.getElementById('add-input-type').value;
    const name = document.getElementById('add-input-name').value;
    const other = document.getElementById('add-input-other').value;

    document.getElementById('add-input-type').value = '';
    document.getElementById('add-input-name').value = '';
    document.getElementById('add-input-other').value = '';

    $.ajax({
        url: `${getBaseURL()}/hardware`,
        type: `PUT`,
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify({
            type: type,
            name: name,
            other: other
        })
    }).then(() => {
        listHardware();
    });
}

/**
 * Aktualisiert die Liste der Komponenten in der Hardware-Tabelle über eine API-Anfrage.
 */
function listHardware() {
    $.ajax({
        url: `${getBaseURL()}/hardware`
    }).then((data) => {
        let table = document.getElementById('hardware-table');
        table.innerHTML = '';
        data.forEach((component) => {
            let row = document.createElement('tr');
            fillRow(row, component);
            table.appendChild(row);
        });
    });
}
// Aktuell bearbeitete Komponente. Stellt sicher, dass immer nur eine Komponente gleichzeitig bearbeitet wird.
let currentlyEditingRow;
let currentlyEditingComponent;

/**
 * Ersetzt eine bestimmte Zeile in der Hardwaretabelle durch bearbeitbare Textfelder und einen
 * Bestätigungs-Button, mit dem die Bearbeitung über #completeEditing(row, component) abgeschlossen
 * werden kann.
 *
 * @param row die zu bearbeitende Tabellenzeile
 * @param component die in der Zeile angezeigte Komponente
 */
function editComponent(row, component) {
    if (currentlyEditingRow && currentlyEditingComponent) {
        completeEditing(currentlyEditingRow, currentlyEditingComponent);
    }

    currentlyEditingRow = row;
    currentlyEditingComponent = component;

    row.innerHTML = '';
    fillCell(row, component.type, true, "type");
    fillCell(row, component.name, true, "name");
    fillCell(row, component.other, true, "other");

    let button = document.createElement('button');
    button.className = 'btn btn-primary';
    button.innerHTML = 'Bestätigen';
    button.type = 'button';
    button.onclick = () => {
        completeEditing(row, component);
    };

    let div = document.createElement('td');
    div.appendChild(button);

    row.appendChild(div);
}

/**
 * Beendet die Bearbeitung einer Tabellenzeile, indem die Inhalte der Textfelder ausgelesen und an die API
 * übersendet werden. Im Anschluss wird die Tabellenzeile wieder mit normalem Text aufgefüllt.
 *
 * @param row die zu bearbeitende Zeile
 * @param component die Komponente, die in der Zeile angezeigt wird (dient der Ermittlung der cID)
 */
function completeEditing(row, component) {
    currentlyEditingComponent = null;
    currentlyEditingRow = null;

    component.type = document.getElementById('input-type').value;
    component.name = document.getElementById('input-name').value;
    component.other = document.getElementById('input-other').value;

    $.ajax({
        url: `${getBaseURL()}/hardware/${component.cID}`,
        type: `POST`,
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(component)
    }).then(() => {
        fillRow(row, component);
    });
}

/**
 * Füllt eine einzelne Tabellenzeile mit nicht bearbeitbaren Daten und den Schaltflächen daneben auf.
 * Von completeEditing() und listHardware() verwendet.
 *
 * @param row die aufzufüllende Tabellenzeile
 * @param component die Komponente, die zukünftig in dieser Zeile angezeigt werden soll
 */
function fillRow(row, component) {
    row.innerHTML = '';
    fillCell(row, component.type);
    fillCell(row, component.name);
    fillCell(row, component.other);
    {
        let editButton = document.createElement('button');
        editButton.innerHTML = 'Bearbeiten';
        editButton.type = 'button';
        editButton.className = 'btn btn-primary mr-2';
        editButton.onclick = () => {
            editComponent(row, component);
        };

        let deleteButton = document.createElement('button');
        deleteButton.innerHTML = 'Löschen';
        deleteButton.type = 'button';
        deleteButton.className = 'btn btn-danger';
        deleteButton.onclick = () => {
            deleteComponent(component.cID);
        };

        let div = document.createElement('td');
        div.appendChild(editButton);
        div.appendChild(deleteButton);

        row.appendChild(div);
    }
}

/**
 * Füllt eine einzelne Tabellenzelle mit bestimmten Daten auf.
 * Von fillRow() und editComponent() benutzt.
 *
 * @param row die Reihe, zu der die Zelle gehören soll (wird ans Ende angehängt)
 * @param data die Daten, die die Zelle anzeigen soll
 * @param editable "true", wenn anstelle eines gewöhnlichen Textes ein Textfeld erzeugt werden soll
 * @param dataName wenn "editable": bestimmt den Namen des erzeugten Textfeldes ("input-<dataName>") für die spätere Abfrage
 */
function fillCell(row, data, editable = false, dataName = null) {
    let element = document.createElement('td');
    if (editable) {
        element.innerHTML = `<input type="text" class="form-control form-inline" id="input-${dataName}" value="${data}">`;
    } else {
        element.innerHTML = data;
    }
    row.appendChild(element);
}


///////////////////////////////////////// API-Anfragen //////////////////////////////////////////////////////////////

/**
 * Sendet eine Anfrage an die API, den Scharfstellstatus umzuschalten.
 */
function toggleArm() {
    $.ajax({
        url: `${getBaseURL()}/status/arm`,
        type: 'POST'
    });
}

/**
 * Sendet eine Anfrage an die API, den Alarm zurückzusetzen.
 */
function resetAlarm() {
    $.ajax({
        url: `${getBaseURL()}/status/reset`,
        type: 'POST'
    });
}

/**
 * Sendet eine Anfrage an die API, den Alarm auszulosen.
 */
function triggerAlarm() {
    $.ajax({
        url: `${getBaseURL()}/status/trigger`,
        type: 'POST'
    })
}

/**
 * Sendet eine Anfrage an die API, die Views neu einzulesen.
 */
function reloadSystem() {
    $.ajax({
        url: `${getBaseURL()}/reload`,
        type: 'POST'
    })
}

/**
 * Sendet eine Anfrage an die API, das Alarmsystem zu stoppen.
 */
function stopSystem() {
    $.ajax({
        url: `${getBaseURL()}/stop`,
        type: 'POST'
    })
}

/**
 * Sendet nach einer Bestätigung über confirm() eine Anfrage an die API, die JVM mit dem Alarmsystem sofort zu beenden.
 */
function killSystem() {
    if (!confirm("Senden des Kill-Requests mit \"OK\" bestätigen."))
        return;
    $.ajax({
        url: `${getBaseURL()}/kill`,
        type: 'POST'
    })
}

/**
 * Sendet eine Anfrage an die API, eine bestimmte Komponente aus der Datenbank zu löschen.
 *
 * @param cID die ID der zu löschenden Komponente
 */
function deleteComponent(cID) {
    $.ajax({
        url: `${getBaseURL()}/hardware/${cID}`,
        type: `DELETE`
    }).then(listHardware);
}

/**
 * Liefert die URL, an die spezielle API-Anfragen angehängt werden können, ohne jedes Mal neu den Host zu bestimmen.
 * @returns {string}
 */
function getBaseURL() {
    return `http://${window.location.host}:8080/api`;
}
